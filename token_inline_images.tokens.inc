<?php

/**
 * @file
 * Token hook implementations for Token Inline Images.
 */

/**
 * Implements hook_token_info().
 */
function token_inline_images_token_info() {
  $info = array();

  // Node tokens.
  $info['tokens']['node']['inline-images'] = array(
    'name' => t('Inline images'),
    'description' => t('The inline images of a node'),
  );

  return $info;
}

/**
 * Implements hook_tokens().
 */
function token_inline_images_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();
  $sanitize = !empty($options['sanitize']);

  // Node tokens.
  if ($type == 'node' && !empty($data['node'])) {
    $node = $data['node'];

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'inline-images':
          $node_wrapper = entity_metadata_wrapper('node', $node);

          $fields = $node_wrapper->getPropertyInfo();

          $text = '';
          foreach ($fields as $field_name => $field_info) {
            if (empty($field_info['field'])) {
              continue;
            }

            $type = entity_property_extract_innermost_type($field_info['type']);
            if ($type != 'text_formatted') {
              continue;
            }

            $value = $node_wrapper->{$field_name}->value();
            if (!empty($value)) {
              $text .= $node_wrapper->{$field_name}->value->value();
            }
          }

          preg_match_all('/<img.+?src=[\'"]([^\'"]+)[\'"].*?>/i', $text, $matches);

          if (isset($matches[1])) {
            $urls = array();
            foreach ($matches[1] as $index => $src) {
              $url = url(ltrim($src, "/"), array(
                'absolute' => TRUE,
                'language' => (object) array('language' => FALSE),
              ));
              $urls[] = $sanitize ? check_url($url) : $url;
            }
          }

          if (isset($urls)) {
            $replacements[$original] = implode(",", $urls);
          }

          break;
      }
    }
  }

  return $replacements;
}
